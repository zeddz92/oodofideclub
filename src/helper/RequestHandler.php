<?php


namespace OodoFideclub\RequestHandler;
use GuzzleHttp\Client;
ini_set('error_reporting', -1);
ini_set('display_errors', 1);

class RequestHandler
{
    const url = "https://demo.osiell.com:8443/fideclub/";
    protected $curl;
    protected $client;

    public function __construct()
    {
        $this->curl = curl_init();
        //$this->client = new Client();
    }


    public function post($uri, $data)
    {

       /* $request = $this->client->request('post', 'https://demo.osiell.com:8443/fideclub/customers', [
            'form_params' => [
                'field_name' => 'abc',
                'other_field' => '123',
                'nested_field' => [
                    'nested' => 'hello'
                ]
            ]]);
        $request->setPort(8443);
        //$request->setBody($data); #set body!
        $response = $request->send();

        return $response;*/

        $this->curlOptions($uri, $data);
        $response = curl_exec($this->curl);
        $err = curl_error($this->curl);
        curl_close($this->curl);
        if ($err)
        {
            return "cURL Error #:" . $err;
        }
        else
        {
            return $response;
        }
    }

    private function curlOptions($uri, $data_string)
    {


        curl_setopt($this->curl, CURLOPT_PORT, "8443");

        curl_setopt($this->curl, CURLOPT_URL, self::url . $uri);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(($data_string)))
        );
    }

}