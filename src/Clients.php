<?php


namespace OodoFideclub;

use OodoFideclub\RequestHandler;
use OodoFideclub\DatabseConnection;
require_once "helper/RequestHandler.php";
require_once "config/databse.php";

class Clients
{
    public $conn;

    public function __construct()
    {
        $this->conn = new DatabseConnection\DatabseConnection();
    }

    public function createOdooUser($user)
    {
        $request = new RequestHandler\RequestHandler();
        $query = $this->conn->executeQuery($this->getUserQuery($user->id));
        $user = mysqli_fetch_assoc($query);

        $user = $this->createJsonBodyFor($user);

        $response = $request->post('customers', $user);

        return $response;
    }

    protected function getUserQuery($userId)
    {
        $query = "SELECT 
            users.`category_id`,
            client_categories.`en` as category_name,
            users.`firstname`,
            users.`owner_phone`,
            store_config.value as address,
            users.`lastname`,
            users.id as id_client,
            users.`phone_company`,
            users.`company_name`,
            users.`country_code`,
            users.`subdomain`,
            users.`email`
        
            FROM `users` 
            LEFT JOIN `client_categories` on `users`.category_id = `client_categories`.id
            LEFT JOIN `store_config` on (`users`.id = `store_config`.user_id and `store_config`.name = 'store_address')
            WHERE users.id = '$userId'
        ";

        return $query;
    }

    private function createJsonBodyFor($user)
    {
        $data = "{
          \"jsonrpc\": \"2.0\",
          \"id\": ".$user['id_client'].",			
          \"method\": \"call\",
          \"params\": {
            \"category\": [".$user['category_id'].", \"".$user['category_name']."\"],
            \"firstname\": \"".$user['firstname']."\",
            \"mobile\": \"".$user['owner_phone']."\",
            \"lastname\": \"".$user['lastname']."\",
            \"id\": ".$user['id_client'].",				
            \"phone\": \"".$user['phone_company']."\",
            \"company_name\": \"".$user['company_name']."\",
            \"country_code\": \"".$user['country_code']."\",
            \"address\": \"".$user['address']."\",
            \"subdomain\": \"".$user['subdomain']."\",
            \"salesman\": [
              [".$user['id_client'].", \"".$user['firstname']."\"]
            ],
            \"email\": \"".$user['email']."\",
            \"plan\": [10, \"Fideclub Plan\"]
          }
        }";

        return $data;
    }


}